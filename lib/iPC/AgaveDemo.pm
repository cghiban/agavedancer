package iPC::AgaveDemo;
use Dancer ':syntax';

#use Dancer::Plugin::MemcachedFast;
use Agave::Client ();
use Agave::Client::Client ();
use Data::Dumper;

our $VERSION = '0.2';

# TODO - this needs work
sub token_valid {

	my $tk_expiration = session('token_expiration_at');
	# if we don't have an expiration token
	return 0 unless $tk_expiration;

    my $now = time();
    if ($tk_expiration < $now) {
        session 'logged_in' => undef;
        return 0;
    }
	return 1;
}


sub genomes  {
	
	{
		arabidopsis_lyrata => {
			sub_part => 'araly1',
			versions => [qw/v1.0/],
		},
		arabidopsis_thaliana => {
			sub_part => 'col-0',
			versions => [qw/v10 v9/],
		},
		brachypodium_distachyon => {
			sub_part => 'bd21',
			versions => [qw/v1.0/],
		},
		oryza_indica => {
			sub_part => 'bgi05',
			versions => [qw/v1.0/],
		},
		oryza_japonica => {
			sub_part => 'nipponbare',
			versions => [qw/v6/],
		},
		physcomitrella_patens => {
			sub_part => 'phypa1',
			versions => [qw/v1.1/],
		},
		populus_trichocarpa => {
			sub_part => 'nisqually1',
			versions => [qw/v2.0/],
		},
		sorghum_bicolor => {
			sub_part => 'bt623',
			versions => [qw/v1/],
		},
		vitis_vinifera => {
			sub_part => 'pn40024',
			versions => [qw/v12x/],
		},
		zea_mays => {
			sub_part => 'b73',
			versions => [qw/v5a v2 v1/],
		}
	};
};

sub _auth {
    my ($u, $p) = @_;
    my $client = _build_client($u, $p);
    my $apio = eval {
            Agave::Client->new(
                username  => $u,
                password  => $p,
                apisecret => $client->{consumerSecret},
                apikey    => $client->{consumerKey},
            )};
    print STDERR 'auth failed: ', $@, $/ if $@;

    return $apio;
};

sub _build_client {
    my ($u, $p, $purge) = @_;

    my $client;

    # check if we have an Agave client already
    my $apic = Agave::Client::Client->new({ 
        username => $u, 
        password => $p, 
        debug => 0,
    });

    my $client_name = 'AGAVEDEMO';

    #if ($purge) {
    #    print STDERR '** ', __PACKAGE__, ' purging client ', 
    #        $client_name, ' for user ', $u, $/;
    #    eval {$apic->delete($client_name);};
    #    print STDERR  '** ', __PACKAGE__, ' deleting: ', $@, $/ if $@;
    #}
    #else {
    #    $client = $apic->client( $client_name );
    #}
    #
    #my ($user) = DNALC::User->search(username => $u);
    my $user = undef;  # will not use it in this demo


    # force creation of the client, this will give us the key and secret
    if ($client) {
        if ($user) { # we should always have a user
            $client->{consumerSecret} = $user->password;
        }
    }
    else {
        $client = $apic->create({name => $client_name});
        if ($client) { 
            # store the secret;
            #$user->password( $client->{consumerSecret} );
            #$user->update;
        }
    }

    return $client;
};

get '/' => sub {
    template 'index';
};

get '/logout' => sub {
	session 'token' => '';
	session 'logged_in' => 0;

	return redirect '/';
};

any ['get', 'post'] => '/login' => sub {
	
	my $err = "";

	if ( request->method() eq "POST" ) {
		if ( params->{'username'} eq "") {
			$err .= "Username is missing";
		}
        my $api = eval {_auth(params->{'username'}, params->{'password'})};
        if ($@) {
            print STDERR  "Error: ", $@, $/;
        }
        
		if ($api && $api->token) {

            #if ($api->auth->{token_expires_at} - time() < 1000) {
            #    my $new_token = $api->auth->refresh();
            #    if (defined $new_token && $new_token ne '') {
            #        $api->token($new_token);
            #    }
            #}

            debug "Token: " . $api->token . "\n";
            session 'username' => params->{'username'};
            session 'token' => $api->token;
            session 'logged_in' => 1;
            session 'token_expiration_in' => $api->auth->token_expiration_in;
            session 'token_expiration_at' => $api->auth->token_expiration_at;

            print STDERR "Delta: ", $api->auth->token_expiration_in, $/;

            return redirect "/browse/";
		}
		else {
			$err .= "Invalid credentials."
		}
	}

    template 'login', {
		err => $err,
		username => params->{'username'} ? params->{'username'} : session("username"),
	};
	
};

get qr{/browse/?(.*)} => sub {
	unless(session('logged_in') && token_valid()) {
		return redirect '/login';
	}

	my $username = session('username');
	my $api = Agave::Client->new(
					username => $username,
					token => session('token'),
				);

	my ($path) = splat;
	my $path_to_read = $path ? $path : $username;
	$path_to_read = '/' . $path_to_read unless $path_to_read =~ m|^/|;
    #print STDERR  "PATH: $path_to_read", $/;

	my $io = $api->io;
	my $dir_list = $io->readdir($path_to_read);

    #print STDERR Dumper($dir_list);

	template 'browse', {
		path => $path_to_read,
		list => $dir_list,
	};
};

get qr{/apps/?} => sub {
	unless(session('logged_in') && token_valid()) {
		return redirect '/login';
	}

	my $username = session('username');
	my $apif = Agave::Client->new(
					username => $username,
					token => session('token'),
				);

	my $apps = $apif->apps;
    #my $app_list = memcached_get_or_set ("apps-list-$username", sub { $apps->list;});
    my $app_list = $apps->list;
	#print STDERR Dumper($app_list);

 	template 'apps', {
 		list => $app_list,
	};
};


get '/app/:id' => sub {
	unless(session('logged_in') && token_valid()) {
		return redirect '/login';
	}

	my $username = session('username');
	my $api = Agave::Client->new(
					user => $username,
					token => session('token'),
				);

	my $app_name = param("id");
	my $apps = $api->apps;
	my ($app) = $apps->find_by_id($app_name); 

	my ($inputs, $parameters) = ([], []);
	if ($app) {
		$inputs = $app->inputs;
		$parameters = $app->parameters;
	}
 	template 'app', {
 		app => $app,
		app_inputs => $inputs,
		app_params => $parameters,
		name => param("name"),
	};
};

get '/jobs/?' => sub {
    unless(session('logged_in') && token_valid()) {
		return redirect '/login';
	}

	my $username = session('username');
	my $apif = Agave::Client->new(
					username => $username,
					token => session('token'),
				);

	my $job_ep = $apif->job;
	my $job_list = $job_ep->jobs;
	#print STDERR Dumper($app_list);

 	template 'jobs', {
 		list => $job_list,
	};
};

get '/job/:id' => sub {
	unless(session('logged_in') && token_valid()) {
		return redirect '/login';
	}

	my $job_id = param("id");

	my $username = session('username');
	my $apif = Agave::Client->new(
					username => $username,
					token => session('token'),
				);

	my $job_ep = $apif->job;
	my $job = eval { $job_ep->job_details($job_id);};
    if ($@) {
        print STDERR $@, $/;
        if ($@ =~ /token (?:expired|inactive)/i || $@=~ /invalid credentials/i) {
            return $@;
        }
    }

    if ($job) {
       return template 'job', {
            job => $job,
            job_id => $job_id,
        };
    }
    else {
        return "Job not found! ( Perhaps you should wait a few seconds?! )";
    }
};

get '/job/:id/remove' => sub {
	unless(session('logged_in') && token_valid()) {
		return redirect '/login';
	}

	my $job_id = param("id");
	my $username = session('username');
	my $apif = Agave::Client->new(
					username => $username,
					token => session('token'),
				);

	my $job_ep = $apif->job;
	my $st = $job_ep->delete_job($job_id);

	return redirect '/jobs';
};

any ['get', 'post'] => '/job/new/:id' => sub {
	unless(session('logged_in') && token_valid()) {
		return redirect '/login';
	}

	my @err = ();
	my $app_id = param("id");
	my $username = session('username');
	my $apif = Agave::Client->new(
					user => $username,
					token => session('token'),
				);

	my $apps = $apif->apps;

	my ($app) = $apps->find_by_id($app_id);

	my ($inputs, $parameters) = ([], []);
	if ($app) {
		$inputs = $app->inputs;
		$parameters = $app->parameters;
	}
	my $genomes = genomes();

	my $form = {};
	if ( request->method() eq "POST" ) {
		$form = params();
		#$form->{archive} = 1;

		# TODO - check arguments

		if (defined $form->{genome} && $form->{genome}) {
			my ($g, $v) = split '/', $form->{genome};
			$form->{genome} = "/shared/iplantcollaborative/genomeservices/legacy/0.30/genomes/" 
				. $g . "/" . $genomes->{ $g }->{sub_part} . '/' . $v . '/genome.fas';
			#print STDERR  "Genome: ", $form->{genome}, $/;
			if (grep {$_->{id} eq 'annotation'} @$inputs) {
				$form->{annotation} = "/shared/iplantcollaborative/genomeservices/legacy/0.30/genomes/" 
					. $g . "/" . $genomes->{ $g }->{sub_part} . '/' . $v . '/annotation.gtf';
				#print STDERR  "Annotation..: ", $form->{annotation}, $/;
			}
		}

		my $job_ep = $apif->job;
		my $st = eval { $job_ep->submit_job($app, %$form); };
        if ($@) {
            print STDERR 'Error: ', $@, $/;
        }
        print STDERR Dumper( $st ), $/;
        if ($st) {
            if ($st->{status} eq 'success') {
                my $job = $st->{data};
                return redirect '/job/' . $job->{id};
            }
            else {
                push @err, $st->{message};
            }
        }
        # else
		print STDERR Dumper( $form ), $/;
	}
 	template 'job_new', {
		errors => \@err,
 		app => $app,
		app_inputs => $inputs,
		app_params => $parameters,
		name => $app_id,
		genomes => $genomes,
		username => $username,
		form => $form,
	};
};



true;
